package com.daly.firestoreclass.acitivities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*
import com.google.firebase.firestore.FirebaseFirestoreSettings
import android.widget.ArrayAdapter
import com.daly.firestoreclass.R


class MainActivity : AppCompatActivity() {


    var firebaseStore: FirebaseFirestore = FirebaseFirestore.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val settings = FirebaseFirestoreSettings.Builder()
            .setTimestampsInSnapshotsEnabled(true)
            .build()
        firebaseStore.setFirestoreSettings(settings)


        var cities = arrayListOf("Amman", "Zarqa", "Irbid", "Aqaba")

        mSpinner.adapter =
                ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, cities)

        mButtonAddUser.setOnClickListener { view ->


            //Get values.
            var firstName = mEditTextFirstName.text.toString().trim()
            var lastName = mEditTextLastName.text.toString().trim()
            var mobileNumber = mEditTextMobileNumber.text.toString().trim()
            var emailAddress = mEditTextEmailAddress.text.toString().trim()
            var cityName = mSpinner.selectedItem.toString()


            //Validation
            var result = validation()

            if (result) {
                //Store data in FireStore


                val values = HashMap<String, Any>()
                values.put("first_name", firstName)
                values.put("last_name", lastName)
                values.put("mobile_number", mobileNumber)
                values.put("emailAddress", emailAddress)
                values.put("city_name", cityName)

                firebaseStore.collection("users")
                    .add(values)
                    .addOnSuccessListener { documentReference ->
                        Toast.makeText(this, "ID :" + documentReference.id, Toast.LENGTH_LONG).show()


                    }
                    .addOnFailureListener { e ->
                        Toast.makeText(this, "Error", Toast.LENGTH_LONG).show()
                    }
            }
        }
    }


    fun validation(): Boolean {

        var result: Boolean = true
        var firstName = mEditTextFirstName.text.toString().trim()
        var lastName = mEditTextLastName.text.toString().trim()
        var mobileNumber = mEditTextMobileNumber.text.toString().trim()
        var emailAddress = mEditTextEmailAddress.text.toString().trim()


        if (firstName.isEmpty()) {
            result = false
            mEditTextFirstName.error = "Please enter your first name"
        }
        if (lastName.isEmpty()) {
            result = false
            mEditTextLastName.error = "Please enter your first name"
        }
        if (emailAddress.isEmpty()) {
            result = false
            mEditTextEmailAddress.error = "Please enter your first name"
        }
        if (mobileNumber.isEmpty()) {
            result = false
            mEditTextMobileNumber.error = "Please enter your first name"
        }

        return result
    }
}
