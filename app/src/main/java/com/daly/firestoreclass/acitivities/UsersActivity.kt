package com.daly.firestoreclass.acitivities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.daly.firestoreclass.R
import com.daly.firestoreclass.adapters.UserAdapter
import com.daly.firestoreclass.models.User
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import kotlinx.android.synthetic.main.activity_users.*

class UsersActivity : AppCompatActivity() {


    var firebaseStore: FirebaseFirestore = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users)

        val settings = FirebaseFirestoreSettings.Builder()
            .setTimestampsInSnapshotsEnabled(true)
            .build()
        firebaseStore.setFirestoreSettings(settings)


        getUsers()


//        mListView.adapter = UserAdapter(applicationContext,)
    }


    fun getUsers() {


        firebaseStore.collection("users").get().addOnSuccessListener { documents ->

            var users = ArrayList<User>()

            documents.forEach { document ->

                var firstName: String = document.data.get("first_name") as String
                var lastName: String = document.data.get("last_name") as String
                var mobileNumber: String = document.data.get("mobile_number") as String
                var emailAddress: String = document.data.get("emailAddress") as String
                var cityName: String = ""
                if (document.data.get("city_name") != null) {
                    cityName = document.data.get("city_name") as String
                }

                users.add(
                    User(
                        document.id,
                        firstName,
                        lastName,
                        cityName,
                        mobileNumber,
                        emailAddress
                    )
                )


            }

            mListView.adapter = UserAdapter(applicationContext, users)


            mListView.setOnItemClickListener { parent, view, position, id ->

                var user: User = users.get(position)

                var intent: Intent = Intent(this, UpdateUserActivity::class.java)

                intent.putExtra("user", user)

                startActivity(intent)

            }



        }.addOnFailureListener { ex ->

            Toast.makeText(applicationContext, ex.message, Toast.LENGTH_LONG).show()
        }

    }
}
