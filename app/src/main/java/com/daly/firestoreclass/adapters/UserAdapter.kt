package com.daly.firestoreclass.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Toast
import com.daly.firestoreclass.R
import com.daly.firestoreclass.models.User
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.user_template.view.*

class UserAdapter(var mContext: Context, var users: ArrayList<User>) : BaseAdapter() {


    override fun getCount(): Int {
        return users.size
    }

    override fun getItem(position: Int): User {
        return users.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        var mViewHolder: ViewHolder
        var mView: View


        if (convertView == null) {
            var layoutInflater: LayoutInflater = LayoutInflater.from(mContext)
            mView = layoutInflater.inflate(R.layout.user_template, parent, false)
            mViewHolder = ViewHolder(mView)
            mView.tag = mViewHolder
        } else {
            mView = convertView
            mViewHolder = mView.tag as ViewHolder
        }


        var user: User = getItem(position)
        mViewHolder.mView.mTextViewFullName.text = user.firstName + " " + user.lastName
        mViewHolder.mView.mTextViewId.text = user.id

        mViewHolder.mView.mButtonDelete.setOnClickListener { view ->
            var firebaseStore: FirebaseFirestore = FirebaseFirestore.getInstance()


            var document = firebaseStore.collection("users").document(
                user.id
            )

            document.delete().addOnSuccessListener { it ->

                users.removeAt(position)
                notifyDataSetChanged()
                Toast.makeText(mContext, "Deleted", Toast.LENGTH_LONG).show()
            }
        }

        return mView

    }


    class ViewHolder(var mView: View)
}