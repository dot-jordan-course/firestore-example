package com.daly.firestoreclass.models

import java.io.Serializable

class User(
    var id: String,
    var firstName: String,
    var lastName: String,
    var cityName: String,
    var mobileNumber: String,
    var emailAddress: String
) : Serializable {
}